//Soal No. 1 (Range) 
console.log("no.1 \n");

var number1=1;
var number2=10;

function range(startNum, finishNum) {
    let rangeArray = [];
    
    if (startNum<finishNum){

        for(let i=startNum;i<=finishNum;i++){
            rangeArray.push(i);
        }
    }
    else if (startNum>finishNum){
        for(let i=finishNum;i<=startNum;i++){
            rangeArray.push(i);
        }
        
        rangeArray.sort(function (min, max) { return max - min } ) ;
        
    }
    else if (!startNum || !finishNum) {
        rangeArray = -1;
    }

    return rangeArray;
}

console.log(range(number1,number2));

//Soal No. 2 (Range with Step)

console.log("no.2 \n");

function rangeWithStep(startNum, finishNum, step) {
    let rangeArrayNo2 = [];

    if (startNum<finishNum){
        var tmpNum=startNum;

        for (var i = 0; tmpNum <= finishNum; i++) {
            rangeArrayNo2.push(tmpNum)
            tmpNum +=step
        }
    }
    else if(startNum>finishNum){
        var tmpNum=startNum;

        for (var i = 0; tmpNum >= finishNum; i++) {
            rangeArrayNo2.push(tmpNum);
            tmpNum -=step;
        }
        
        rangeArrayNo2.sort(function (min, max) { return max - min } ) ;
    }
    else if (!startNum || !finishNum){
        return -1;
    }

    return rangeArrayNo2;

}

console.log(rangeWithStep(11, 23, 3));

//Soal No. 3 (Sum of Range)

console.log("no.3 \n");

function sum(startNum, finishNum, step){

    if(!startNum && !finishNum && !step){
        return 0;
    }
    if(!step){
        step=1;
    }


    if(startNum && finishNum)
    {
        var sumArray = rangeWithStep(startNum, finishNum, step);
        let total=0;

        for(var i=0;i<sumArray.length;i++)
        {
            total=total+sumArray[i];
        }

        return total;
    }
    else if(!startNum || !finishNum){
        return startNum;
    }
    
    

}

console.log(sum(1,10));


//Soal No. 4 (Array Multidimensi)

console.log("no.4 \n");

function dataHandling(data){
    for(var i=0;i<data.length;i++){
        var id = "Nomor Id : " +data[i][0];
        var nama = "Nama Lengkap : "+data[i][1];
        var ttl = "TTL : "+data[i][2] + " " + data[i][3];
        var hobi = "Hobi : "+data[i][4];

        console.log(id);
        console.log(nama);
        console.log(ttl);
        console.log(hobi);
        console.log('\n');
    }

}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);

//Soal No. 5 (Balik Kata)
console.log("no 5 \n");

stringKata = 'SanberCode';

function balikKata(kata) {
    let string = '';
    for (var i = kata.length - 1; i >= 0; i--) {
        string += kata[i];
    }
    return string;
}

console.log(balikKata(stringKata));


//Soal No. 6 (Metode Array)
console.log("no 6 \n");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(data){
    var newData = data;
    var editName = data[1]+ "Elsharawy";
    var editProvince = "Provinsi" + data[2];
    var gender= "Pria";
    var sekolah = "SMA International Metro";

    newData.splice(1,1,editName);
    newData.splice(2,1,editProvince);
    newData.splice(4,1,gender,sekolah);

    var arrDate = data[3];
    var editDate = arrDate.split('/');
    var monthNum = editDate[1];
    var monthName = "";

    switch (monthNum) {
        case "01":
            monthName = "Januari"
            break;
        case "02":
            monthName = "Februari"
            break;
        case "03":
            monthName = "Maret"
            break;
        case "04":
            monthName = "April"
            break;
        case "05":
            monthName = "Mei"
            break;
        case "06":
            monthName = "Juni"
            break;
        case "07":
            monthName = "Juli"
            break;
        case "08":
            monthName = "Agustus"
            break;
        case "09":
            monthName = "September"
            break;
        case "10":
            monthName = "Oktober"
            break;
        case "11":
            monthName = "November"
            break;
        case "12":
            monthName = "Desember"
            break;
        default:
            break;
    }

    var dateJoin = editDate.join("-");

    var dateArr = editDate.sort(function(value1, value2){value2-value1});

    var newName = editName.slice(0,15);
    console.log(newData);
    console.log(monthName);
    console.log(dateArr);
    console.log(dateJoin);
    console.log(newName);


    
    


}




