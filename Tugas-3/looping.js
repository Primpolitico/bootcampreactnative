//No. 1 Looping While
var i=2;

console.log("LOOPING PERTAMA");

while(i<=18){
    console.log(i+" - I love coding");
    i=i+2;
}

console.log("LOOPING KEDUA");

while(i>=2){
    console.log(i+" - I will become a mobile developer");
    i=i-2;
}

console.log("\n No.2 Looping for \n");


for(var iNomor2=1;iNomor2<21;iNomor2++){

    if(iNomor2%2==0){
        console.log(iNomor2+" - Berkualitas");
    }
    else{
        iNomor2%3==0 ? console.log(iNomor2+" - I Love Coding "):console.log(iNomor2+" - Santai"); 

    }

}

//No.3 
console.log("\n No. 3 Membuat Persegi Panjang # \n");

for(var iNomor3=0;iNomor3<4;iNomor3++)
{
    let kotak = '';
    for(var jNomor3=0;jNomor3<8;jNomor3++)
    {
        kotak += '#';
    }
    console.log(kotak);
}

console.log("\n No. 4 Membuat Tangga \n");

let tangga="";
for(var iNo4=0;iNo4<7;iNo4++){
    tangga +="#";
    for(var jNo4=0;jNo4<7;jNo4++){
        tangga +="";
    }
    console.log(tangga);
}

console.log("\n No. 5 Membuat Papan Catur \n");

for(var iNomor5=0;iNomor5<8;iNomor5++){
    let catur='';
    for(var jNomor5=0;jNomor5<8;jNomor5++){
        if(iNomor5 % 2 != 0){
            jNomor5 % 2 == 0 ? catur += '#' : catur += ' ';
        }
        else{
            jNomor5 % 2 != 0 ? catur += '#' : catur += ' ';
        }
    }
    console.log(catur);
}

