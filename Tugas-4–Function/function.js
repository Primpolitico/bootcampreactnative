//No.1
//Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console.

console.log('no. 1 \n');
function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak()) // "Halo Sanbers!" 

//No.2
//Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.

console.log('no.2 \n');

function kalikan(param1,param2){
  return param1*param2;
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log('no.3 \n');

function introduce(nama, age, address, hobby){
  return "Nama saya " +nama +", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!"
} 

var nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(nama, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
