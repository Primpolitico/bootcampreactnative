import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TextInput,ScrollView,TouchableOpacity} from 'react-native';
import AboutMe from './Tugas/Tugas-13-Styling-Flex/AboutScreen';
import Flexbox from './Tugas/Tugas-13-Styling-Flex/LoginScreen';
import AppNote from './Tugas/Tugas-14/App';
import Main from './Tugas/Tugas-14/components/Main';


export default function App() {
  return (
      <Main/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
