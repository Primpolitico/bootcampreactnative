import React from 'react';
import {
    StyleSheet, Text, View, Image, TouchableOpacity,
    FlatList
  } from 'react-native';

import Login from './components/Login'

  export default function Flexbox (){
    return (
    <View style={styles.container}>
        <Login />
    </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:"white"
    },
  });
