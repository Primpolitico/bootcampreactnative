import React from 'react';
import {
  StyleSheet, Text, View, Image, TouchableOpacity,
  FlatList
} from 'react-native';
import About from './components/about';

export default function AboutMe() {
  return (
    <View style={styles.container}>
      <View>
        <Text>ABOUT ME</Text>
        <Text>Nama lengkap : Prima Politico</Text>
        <Text>Medsos: @primpriim</Text>
        <Text>Portofolio : https://gitlab.com/Primpolitico</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",
    borderColor: 'black',
    borderWidth: 1
  }
});