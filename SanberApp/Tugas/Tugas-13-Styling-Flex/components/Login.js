import React, { Component } from 'react';

import {
    TextInput, StyleSheet, Text, View, TouchableOpacity,Image
} from 'react-native';

export default class Login extends Component {
    render() {
        return (
            <View style={{ width: "80%", height: "80%"}}>

                <Text style={{fontSize:34,textAlign:'center'}}>Who Am I</Text>
                <View>
                    <Image source={require('../images/question_mark.png')} />
                </View>

                <View style={styles.inputView}>

                    <TextInput

                        style={styles.inputText}

                        placeholder="Email…"

                        placeholderTextColor="#003f5c" />

                </View>
                <View style={styles.inputView} >

                    <TextInput

                        style={styles.inputText}

                        placeholder="Password"

                        placeholderTextColor="#003f5c" />

                </View>

                <TouchableOpacity style={styles.loginBtn}>
                    <Text style={styles.loginText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loginBtn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        backgroundColor: 'blue',
    },
    inputView: {

        width: "80%",

        backgroundColor: "#465881",

        borderRadius: 25,

        height: 50,

        marginBottom: 20,

        justifyContent: "center",

        padding: 20

    },
    inputText: {

        height: 50,

        color: "white"

    },

});