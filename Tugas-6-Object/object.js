
//Soal No. 1 (Array to Object)
console.log("no.1 \n");

function arrayToObject(arr) {

    let result={};
    var dateNow=new Date();
    var yearNow= dateNow.getFullYear();
    
    for (var i=0;i<arr.length;i++)
    {
        let tempAge= yearNow-arr[i][3];
        if(isNaN(tempAge)||yearNow<arr[i][3])
        {
            tempAge="Invalid birth year";
        }
        let person = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: tempAge
        }

        let key = i+1 +". "+arr[i][0]+" "+arr[i][1];
        result[key] = person;
    }

    if(Object.entries(result).length === 0){
        return "";
    }

    return result;
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]

console.log(arrayToObject(people));

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log(arrayToObject(people2));

console.log(arrayToObject([]));

//Soal No. 2 (Shopping Time)
console.log("no.2 \n");

function shoppingTime(memberId, money) {
    
    let listPurchased=[];
    let changeMoney= money;
    let payMoney = money;
    let result={};

    const products = {
        "Sepatu brand Stacattu": 1500000,
        "Baju brand Zoro": 500000,
        "Baju brand H&N ": 250000,
        "Sweater brand Uniklooh": 175000,
        "Casing Handphone": 50000
    }

    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }

    if(money < 50000){
        return "Mohon maaf, uang tidak cukup";
    }

    for(var key in products)
    {        
        if(payMoney>=products[key]){
            listPurchased.push(key);

            changeMoney=changeMoney-products[key];
        }
        
    }

    result ={
        "memberId":memberId,
        "money":payMoney,
        "listPurchased":listPurchased,
        "changeMoney":changeMoney
    }
    
    return result;

  }

  // TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


//Soal No. 3 (Naik Angkot)
console.log("no.3 \n");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here

    let penumpang="";
    let naikDari="";
    let tujuan="";
    let bayar=0;
    let result=[];
    
    for(var i=0;i<arrPenumpang.length;i++)
    {
        penumpang=arrPenumpang[i][0];
        naikDari=arrPenumpang[i][1];
        tujuan=arrPenumpang[i][2];
        bayar=(rute.indexOf(tujuan)-rute.indexOf(naikDari))*2000;

        let temp = {
            "penumpang": penumpang,
            "naikDari":naikDari,
            "tujuan": tujuan,
            "bayar": bayar
        };
        
        result[i]=temp;
    }


    return result;

  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
